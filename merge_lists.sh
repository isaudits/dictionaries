#NOTE - to remove lines with non-ascii characters from foreign wordlists use:
# perl -nle 'print if m{^[[:ascii:]]+$}' <filename> | sort | uniq > outfile.txt

echo "By default we will create wordlists with unix-style line endings for linux / mac"
echo "Would you like to create lists with dos-style line endings for Windoze instead? (y/n) [n]"
read dos

if [ $dos == 'y' ]
    then
    cmd='unix2dos -f -n'
else
    cmd='dos2unix -f -n'
fi

echo "will fix line endings with $cmd" 

echo "Combining password lists..."
sort -u passlists/*.* > combined_passlists.tmp
$cmd combined_passlists.tmp combined_passlists.txt
rm combined_passlists.tmp

echo "Combining wordlists..."
sort -u wordlists/*.* > combined_wordlists.tmp
$cmd combined_wordlists.tmp combined_wordlists.txt
rm combined_wordlists.tmp

echo "Applying JTR transforms to wordlists..."
john --wordlist=combined_wordlists.txt --rules --stdout | sort > combined_wordlists_transformed.tmp
$cmd combined_wordlists_transformed.tmp combined_wordlists_transformed.txt
rm combined_wordlists_transformed.tmp

echo "Combining wordlists and password lists to master list..."
sort -u *.txt > combined_all.tmp
$cmd combined_all.tmp combined_all.txt
rm combined_all.tmp

echo "Creating WPA cracking lists from combined list (8-63 chars)..."
cat combined_all.txt | pw-inspector -m 8 -M 63 > combined_wpa.txt
 
read -p "Do you want to create HUGE wordlists using JTR single rule transforms (y/n) [n]?" huge

if [ $huge == 'y' ]
    then
    echo "Applying JTR single rule transforms to wordlists (HUGE!)..."
    john --wordlist=combined_wordlists.txt --rules=single --stdout > huge.tmp
    
    echo "Creating HUGE combined wordlist..."
    sort -u combined_all.txt huge.tmp > combined_all_huge.tmp
    rm huge.tmp
    $cmd combined_all_huge.tmp combined_all_huge.txt
    rm combined_all_huge.tmp
    
    echo "Creating WPA cracking lists from huge list (8-63 chars)..."
    cat combined_all_huge.txt | pw-inspector -m 8 -M 63 > combined_wpa_huge.txt
    
fi
