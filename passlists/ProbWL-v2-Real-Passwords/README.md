Probable-Wordlists
===========

This folder is for source lists downloaded from Probable-Wordlists. Files with a
.txt or .7z extension are ignored by the repo to prevent syncing huge files to
our repo.

The source files to place in here can be downloaded from here:
[Real-Passwords , 7z Compressed - Torrent](Real-Passwords/Real-Password-Rev-2-Torrents/ProbWL-v2-Real-Passwords-7z.torrent)

Original source:

<https://github.com/berzerk0/Probable-Wordlists/blob/master/Downloads.md>
<https://github.com/berzerk0/Probable-Wordlists/>