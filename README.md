dictionaries
===========

Password cracking dictionaries and wordlists

-------------------------------------------------------------------------------

Matthew C. Jones, CPA, CISA, OSCP, CCFE

IS Audits and Consulting, LLC - <http://www.isaudits.com/>

TJS Deemer Dana - <http://www.tjsdd.com>

-------------------------------------------------------------------------------

This is a compilation of dictionaries and wordlists that I've assembled over the
years for conducting legal penetration tests. They are intended only for lawful
and ethical use only in conducting research or legal security assessments.

There are also scripts for combining wordlists from subfolders into larger master
lists (these are excluded from git repo due to size)

##Additional sources:

<https://github.com/berzerk0/Probable-Wordlists>

<https://github.com/danielmiessler/SecLists>

-------------------------------------------------------------------------------
#Wordlist manipulation notes

To remove lines with non-ascii characters from foreign wordlists use:

```perl -nle 'print if m{^[[:ascii:]]+$}' <filename> | sort | uniq > outfile.txt```

merge command from subdirectory & output into parent directory
    
    cat *.* | sort | uniq > tmp.txt
    dos2unix -f -n tmp.txt ../combined_unix.txt
    unix2dos -f -n tmp.txt ../combined_win.txt
    rm tmp.txt

Combine a bunch of files into a sorted file with no duplicates

```cat *.* | sort | uniq > _combined.txt```


Unix and Dos conversion:
-f flag forces even if OS detects a binary file (necessary with wordlists)
-n flag preserves input file

```dos2unix -f <filename>```


Preserve input file (-n):

    dos2unix -f -n infile outfile
    unix2dos -f -n infile outfile
    

Excluding words containing a single character class (e.g. all uppercase, all lowercase)

    egrep -v "^[[:upper:]]*$" test.txt
    egrep -v "^[[:lower:]]*$" test.txt
    egrep -v "^[[:digit:]]*$" test.txt
    egrep -v "^[[:punct:]]*$" test.txt
    egrep -v "^[[:upper:]]*$|^[[:lower:]]*$|^[[:digit:]]*$|^[[:punct:]]*$" test.txt
    

Make list WPA/WPA2 compatible by deleting words less than 8 chars and more than 63
```grep -x '.\{8,63\}' passwordlist.txt > wpaList.txt```


Make list Active Directory default compatible by deleting words less than 7 chars and more than 32
```grep -x '.\{7,32\}' passwordlist.txt > adList.txt```


Make a shorter AD password list by also excluding passwords of a single character class
```grep -x '.\{7,32\}' passwordlist.txt | egrep -v "^[[:upper:]]*$|^[[:lower:]]*$|^[[:digit:]]*$|^[[:punct:]]*$" > adList.txt```


-------------------------------------------------------------------------------
This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program. If not, see <http://www.gnu.org/licenses/>.